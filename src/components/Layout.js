import React from 'react';
import Sidebar from "./Sidebar";
import Nav from "./Nav";

function Layout(props) {
    return (
        <div>
            <div style={{display: "flex", width: "100%"}}>
                <Sidebar history={props.history}/>
                <div style={{maxWidth: '800px', width: "70%"}}>
                    <Nav/>
                    {props.children}
                </div>
            </div>
        </div>
    );
}

export default Layout;
