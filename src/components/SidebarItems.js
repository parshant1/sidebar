const SidebarItems = [
  {
    name: "Dashboard",
    route: "/dashboard",
  },
  {
    name: "Customer Management",
    route: "/customer",
  },
  {
    name: "Service Provider Management",
    route: "/service",
  },
  {
    name: "Revenue Management",
    route: "/revenue",
  },
  {
    name: "Expenditure Management",
    route: "/expenditure",
  },
  {
    name: "Rewards Points Management",
    route: "/reward",
  },
  {
    name: "CMS Pages Management",
    route: "/cms",
  },
  {
    name: "Notification Management",
    route: "/notification",
  },
  {
    name: "Ratings Management",
    route: "/rating",
  },
  {
    name: "Tracking",
    route: "/track",
  },
  {
    name: "Logout",
    route: "/logout",
  },
];

export default SidebarItems;
